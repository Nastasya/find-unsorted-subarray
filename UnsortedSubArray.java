
import org.apache.commons.lang3.tuple.Pair;

public class Test {
    public static void main(String[] args) {

         int[] arr = {4,5,8,15,10,6,17,27,50,25,45,60};

        System.out.println(findUnsortedSubArrayIndexes(arr));
    }
    public static Pair<Integer, Integer> findUnsortedSubArrayIndexes(int[] arr) {

        int leftIndex = findLeftIndex(arr);
        if (leftIndex == -1) {
            return Pair.of(null,null);
        }
        int rightIndex = findRightIndex(arr);

        int min = arr[leftIndex];
        int max = arr[rightIndex];
        for (int i = leftIndex; i <= rightIndex; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
            if (arr[i] > max) {
                max = arr[i];
            }
        }

        int startIndex = findMinimumElement(arr, min, leftIndex);
        int endIndex = findMaximumElement(arr, max, rightIndex);

        return Pair.of(startIndex,endIndex);

    }

    private static int findLeftIndex(int[] arr){
        for (int i = 1; i < arr.length; i++) {
            if(arr[i-1]>arr[i]){
                return i-1;
            }
        }
        return -1;
    }

    private static int findRightIndex(int[] arr){
        for (int i = arr.length-1; i >= 0; i--) {
            if(arr[i]<arr[i-1]){
                return i;
            }
        }
        return -1;
    }

    private static int findMinimumElement(int[] arr, int min, int end){
        for (int i = 0; i <= end; i++) {
            if(arr[i]>min){
                return i;
            }
        }
        return -1;
    }

    private static int findMaximumElement(int[] arr, int max, int end){
        for (int i = arr.length - 1; i >= end; i--) {
            if(arr[i]<max){
                return i;
            }
        }
        return -1;
    }
}


